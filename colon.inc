%define prev_elem 0

%macro colon 2
	%%local_start:
		dq prev_elem
		dq %2
		db %1, 0
		%define prev_elem %%local_start
%endmacro
