section .data

SPACE equ 0x20
TAB equ 0x9
NL equ 0xA
ZR equ 0x30
NN equ 0x39

section .text
global exit
global print_string
global print_error
global print_newline
global string_equals

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .repeat:
        cmp byte[rax + rdi], 0
        je .zero
        inc rax
        jmp .repeat
    .zero:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 2
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    xor rsi, rsi
    .repeat:
        xor rdx, rdx
        mov rcx, 10
        div rcx
        add dl, '0'
        dec rsp
        mov byte [rsp], dl
        inc rsi
        test rax, rax
        jnz .repeat

    mov rax, 1
    mov rdi, 1
    mov rdx, rsi
    mov rsi, rsp
    syscall

    add rsp, rdx

ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jnl print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor	rax, rax
	xor	rcx, rcx
    .comparison:
        mov	dl, byte [rdi + rcx]
        cmp	dl, byte [rsi + rcx]
        jne	.end
        inc	rcx
        cmp	dl, 0
        jne	.comparison
        inc	rax
    .end:
        ret

; Читает символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push 0
    mov rsi, rsp
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
    xor r10, r10
    .cycle:
    	push rcx
	    push rdi
	    push rsi
	    call read_char
	    pop rsi
	    pop rdi
	    pop rcx
	    cmp rax, SPACE
	    je .is_space
	    cmp rax, TAB
    	je .is_space
	    cmp rax, NL
	    je .is_space
	    cmp rax, 0
	    je .success
        inc rcx
        cmp rcx, rsi
        je .error
        inc r10
        mov [rdi + rcx - 1], rax
        jmp .cycle

    .is_space:
        cmp r10, 0
        je .cycle
        mov [rdi + rcx], byte 0

    .success:
        mov rax, rdi
        mov rdx, rcx
        jmp .end

    .error:
	    xor rax, rax

    .end:
    	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor r10, r10
    mov r9, 10
    xor r8, r8
    .cycle:
        mov r10b, byte [rdi + r8]
        cmp r10b, ZR
        jl .end
        cmp r10b, NN
        jg .end
        sub r10b, ZR
        mul r9
        add rax, r10
        inc r8
        jmp .cycle
    .end:
        mov rdx, r8
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    cmp rax, rdx
    jle .cycle
    mov rax, 0
    ret
    .cycle:
        mov r10b, [rdi+rcx]
        mov [rsi+rcx], r10b
        inc rcx
        cmp byte [rsi+rcx], 0
        jne .cycle
    mov rax, rdx
    xor rcx, rcx
	ret

