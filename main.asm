%include "words.inc"
%include "dict.asm"
%include "lib.inc"
global _start

%define sys_write 1
%define std_error 2
%define std_out 1

section .rodata
	no_matches db "No matches"

section .text
	_start:
		sub rsp, 256
		xor rdi, rdi
		xor rax, rax
		mov rsi, rsp
		mov rdx, 255
		syscall
		mov byte[rsp+rax], 0

		lea rcx, [rax+1]
		lea rsi, [rax+rsp]
		lea rdi, [rsp+256]
		std
		rep movsb
		add rsp, 256
		sub rsp, rax
		call print_newline

		mov rdi, rsp
		mov rsi, prev_elem
		push rdi
		push rsi

		call find_word
		pop rsi
		pop rdi
		test rax, rax
		jz .no_matches

		add rax, 8
		mov rdi, qword[rax]
		call print_string
		call print_newline
		xor rdi, rdi
		jmp exit

.no_matches:
	mov rdi, no_matches
	call print_error
	mov rdi, std_out
	jmp exit
